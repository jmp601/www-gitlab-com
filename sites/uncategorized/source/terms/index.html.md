---
layout: markdown_page
title: GitLab Terms of Service
description: "Here you can find information on the terms related to GitLab's Website and your use of GitLab Software"
canonical_path: "/terms/"
---
Thank you for choosing GitLab! For the current terms please see the Current Terms of Use table below. 


The **CURRENT TERMS OF USE** (unless otherwise stated in the Table below) shall apply to net-new and renewal purchases made on, or after, August 1, 2021. If you received an Order Form / Quote, or purchased, prior to August 1, 2021, please see the Agreement History below for the terms applicable to your purchase and/or use of GitLab software. Any GitLab Subscription purchased prior to August 1, 2021 including upgrades and additional Users purchased for that Subscription, will be governed by the Agreement in effect as of the Subscription purchase date. 

## **CURRENT TERMS OF USE**

| Type of Use / Activity  | Applicable Agreement |  
| ------ | ------ |  
| Use of any [GitLab Software Offering](https://about.gitlab.com/pricing/){:data-ga-name="pricing"}{:data-ga-location="body"}, including Free tier | [Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/){:data-ga-name="subscription agreement"}{:data-ga-location="body"} |  
| Use of any GitLab Professional Services  <br /><br />  (_Effective as of November 1, 2021_) | [Professional Services Agreement](https://about.gitlab.com/handbook/legal/professional-services-agreement/){:data-ga-name="professional services agreement"}{:data-ga-location="body"} |  
| Privacy Policy | [GitLab Privacy Policy](https://about.gitlab.com/privacy/){:data-ga-name="privacy"}{:data-ga-location="body"} |  
| GitLab Processing Your Data | [GitLab Data Processing Agreement](https://about.gitlab.com/handbook/legal/data-processing-agreement/){:data-ga-name="data processing agreement"}{:data-ga-location="body"} |  
| Request Removal of Content / Data | [DMCA Notice and Take Down](https://about.gitlab.com/handbook/dmca/){:data-ga-name="dmca"}{:data-ga-location="body"} |  
| Use of GitLab's Website(s)  | [Website Terms of Use](https://about.gitlab.com/handbook/legal/policies/website-terms-of-use/){:data-ga-name="website terms"}{:data-ga-location="body"} |  
| Cookies & Visiting the Website | [GitLab Cookie Policy](https://about.gitlab.com/privacy/cookies/){:data-ga-name="cookies"}{:data-ga-location="body"} |  
| Partner Program | Enroll and Terms please visit the [GitLab Partner Program](https://partners.gitlab.com/English/){:data-ga-name="partner program"}{:data-ga-location="body"} | 
| GitLab for Education Program* | [GitLab for Education Program Agreement](https://about.gitlab.com/handbook/legal/education-agreement/){:data-ga-name="education agreement"}{:data-ga-location="body"} |
| GitLab for Open Source Program** | [GitLab for Open Source Program Agreement](https://about.gitlab.com/handbook/legal/opensource-agreement/){:data-ga-name="open source agreement"}{:data-ga-location="body"} |  

*Only applicable for Educational Institutions using GitLab Software for Instructional Use, or, Non-Commercial Academic Research. Please visit [Program Guidelines](https://about.gitlab.com/solutions/education/join/){:data-ga-name="education"}{:data-ga-location="body"} for more information.

**Only applicable for Open Source Organizations using GitLab Software for Open Source Software. Please visit [Program Guidelines](https://about.gitlab.com/solutions/open-source/join/){:data-ga-name="open source"}{:data-ga-location="body"} for more information.


## **AGREEMENT HISTORY**  

GitLab, including GitLab Legal, is committed to [transparency](https://about.gitlab.com/handbook/values/#transparency){:data-ga-name="transparency"}{:data-ga-location="body"} as part of its’ values, as such we provide previous versions of our Agreements.   

| Legacy Agreement and Applicable Dates | Agreement Location |  
| ------ | ------ |  
| Subscription Agreement (February 1, 2021 - July 31, 2021) | [Legacy Subscription Agreement V2](https://about.gitlab.com/handbook/legal/legacy-subscription-agreement-v2/){:data-ga-name="legacy subscription agreement v2"}{:data-ga-location="body"} |  
| Subscription Agreement (January 1, 2015 - January 31, 2021) | [Legacy Subscription Agreement V1](https://about.gitlab.com/terms/signature.html){:data-ga-name="legacy subscription agreement v1"}{:data-ga-location="body"} | 
| Professional Services Agreement (January 1, 2015 - October 31, 2021) | [Legacy Professional Services Agreement V1](https://about.gitlab.com/handbook/legal/legacypsav1/){:data-ga-name="legacy professional services agreement v1"}{:data-ga-location="body"} |  
