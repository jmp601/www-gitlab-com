---
layout: handbook-page-toc
title: Home Office Equipment and Supplies
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## Home Office Equipment and Supplies

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Home Office Items

Below is a list of common items for your home office at GitLab. It is uncommon that you'll need all of the items listed below. Read [GitLab's guide to a productive home office or remote workspace](/company/culture/all-remote/workspace/), and use your best judgement and buy them as you need them. If you wonder if something is common, feel free to ask IT Ops (and in turn, IT Ops should update the list).

#### Expensing hardware and supplies outside of the US

**The prices in the table below can serve as reference for team members as you look into purchasing new equipment. As an all-remote global company, prices might vary per region depending on taxes, tariffs and other factors. When making a spend above $100 USD, we encourage team members to look into 2 or 3 different providers and compare prices before acquiring new equipment.** 

**For non-US team members: if the product cost is higher in your location, confirm that an equivalent product in the United States would be close to the guideline price in the table below. In the event there are additional taxes or other fees in your region, please note this cost increase when submitting an expense report to ensure an efficient and speedy approval and reimbursement from our third party accounting team.**


| Item                                             | Price Guide in USD (for purchases in the USA)     | Importance | Why                                                                                                                                                                                                                                                                                 |
|--------------------------------------------------|--------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Height-adjustable desk                           | $300 - $500                          | 10/10      | We use our desks everyday and need them to be ergonomic and adjustable to our size and needs, whether that is sitting or standing.                                                                                                                                                  |
| Ergonomic chair                                  | $200 - $400                          | 10/10      | We use our desk chairs hours at a time, and need them to be healthy, supportive, and comfortable.                                                                                                                                                                                   |
| Headphones (wired or wireless, with mic ability) | $100                                 | 10/10      | We use our headphones everyday during our meetings, to connect with our fellow team members. We need our headphones to be comfortable, functional, and great quality. |
| 16:9 external monitor (single or dual monitors)  | $500                                 | 10/10      | Finding a monitor that is large, comfortable to use with sharpness is extremely important for our eyes and health. You can expense up to two 16:9 monitors (1080p or 4K) if they are required/helpful for your role.                                                                |
| Ultra-wide external monitor                      | $1,000                               | 10/10      | Finding a monitor that is large, comfortable to use with sharpness is extremely important for our eyes and health. Some team members prefer a single ultrawide monitor over multiple monitors.                                                                                      |
| Keyboard                                         | $250                                 | 10/10      | Find a keyboard that works for you and is comfortable for your workflow and hand size.                                                                                                                                                                                              |
| Mouse or Trackpad                                | $80 or $145                          | 10/10      | Find a mouse/trackpad that works for you and is comfortable for your workflow.                                                                                                                                                                                                      |
| Laptop stand                                     | $90                                  | 10/10      | Your eyes and head must look at the same angle as you work and so your laptop must be elevated if you are working with an external monitor.                                                                                                                                         |
| Webcam                                           | $80                                  | 9/10       | If you would like a much better image quality than from the camera in your laptop, a webcam can make video conversation better. For those who interface routinely with clients, leads, and external parties, you may also consider a pricier mirrorless or DSLR camera as a webcam. |
| Portable 15" external monitor                    | $200                                 | 9/10       | You have the freedom to work from any location, and having a portable monitor allows that your workflow does not suffer from being constrained to a single small laptop screen.                                                                                                     |
| USB-C Adapter                                    | $80                                  | 9/10       | Most MacBooks only have 1 free USB-C port, so an adapter with additional ports is a necessity.                                                                                                                                                                                      |
| HDMI / monitor cable                             | $15                                  | 9/10       | Find a quality cable so that the connection between your laptop and monitor is healthy and secure.                                                                                                                                                                                  |
| Yubikey                                          | $50  | 8/10       | Per our Security Practices, purchasing Yubikey is not mandatory, but is considered as an extra layer of authentication for better security.                                                                                                                                         |
| Monitor Privacy Filter                           | $80                                  | 8/10       | Important if you work in public places and need to be certain your work cannot be seen.                                                                                                                                                                                             |
| WiFi Router with guest functionality/Powerline/WiFi network extender          | $100                               | 7/10       | If your existing router does not allow for isolating your work notebook from your personal devices in your home network, consider buying a router that does.  You can also choose to extending your home network to your workspace by using a network extender.                                                                                                                    |
| Laptop bag or backpack                           | $60                                  | 7/10       | Carry your laptop and external monitor safely with this travel bag. We recommended you get a bag (or backpack) with straps so the device stays on you when you need your hands free.                                                                                                |
| Laptop cooling pad                               | $35 - $50                            | 6/10       | Useful if you live in a place where summer heat can affect your laptop's ability to keep itself cool                                                                                                                                                                                |
| Dedicated microphone                             | $130                                 | 6/10       | For those who routinely interface with clients, leads, media, and external parties — or create regular content for GitLab channels — you may also consider a dedicated microphone to capture your voice with added richness and detail.                                             |
| Ethernet connector                               | $20                                  | 6/10       | This is if you choose to connect to your internet directly versus by Wi-Fi.             


### <i class="far fa-question-circle" id="biz-tech-icons"></i> Not sure what to buy? Equipment Examples

#### Adapters and cables

##### A note on HDMI and 4K@60Hz

If you need HDMI connectivity (e.g. to connect a TV, projector or monitor) and are looking for a USB-C adapter or hub that supports [4K resolution](https://en.wikipedia.org/wiki/4K_resolution) (3840 × 2160 pixels), be aware that most of the adapters and hubs on the market only support refresh rate of 30 Hz (i.e. 30 frames per second). This is ok for movies, but not great for interactive work. For example, mouse pointer will noticeably jump on a 30 Hz screen when you move it. Scrolling text is even worse. All that puts extra strain on your eyes. There are products that support refresh rate of 60 Hz and they cost roughly the same. Look for 4K@60Hz in product specification. If you bought one and it still does not give you the desired refresh rate, make sure your HDMI cable supports it. [HDMI 2.0](https://en.wikipedia.org/wiki/HDMI#Version_2.0) and newer cables do.

##### USB adapters

  * Falwedi 7-in-1 USB-C Hub - [US](https://www.amazon.com/dp/B083FBYP9B), [Australia](https://www.amazon.com.au/dp/B083FBYP9B). HDMI port supports 4K@60Hz.
  * TOTU 8-in-1 USB-C Hub - [US](https://www.amazon.com/dp/B07FX2LW35/)
  * FLYLAND Hub, 9-in-1 - [Germany](https://www.amazon.de/dp/B00OJY12BY/)
  * VAVA USB-C Hub 8-in-1 Adapter - [Australia](https://www.amazon.com.au/dp/B07JCKCZGJ/)
  * UGREEN Ethernet to USB 3.0 Adapter - [US](https://www.amazon.com/dp/B00MYTSN18/)
  * Yinboti USB-C Hub for New Macbook Pros - [US](https://www.amazon.com/gp/product/B07FMNJC6J/)
  * Kensington UH4000 4 Port USB Hub 3.0 - [US](https://www.amazon.com/Kensington-UH4000-Port-USB-3-0/dp/B00O9RPP28/)
  * YXwin USB C Hub 6-in-1 Adapter including Ethernet - [UK](https://www.amazon.co.uk/YXwin-Adapter-Delivery-1000mbps-Ethernet/dp/B07PSM6RQS/)
  * HyperDrive 4 in 1 USB C Adapter - [India](https://www.amazon.in/HyperDrive-Thunderbolt-Macbook-MacBook-Devices/dp/B01M7O0WF4/)
  * Anker PowerExpand+ 7-in-1 - [US](https://www.amazon.com/Anker-Upgraded-Delivery-Pixelbook-A83460A2/dp/B07ZVKTP53/)

##### USB Docks
  * CalDigit TS3 Plus (**Will require Manager Approval to expense due to cost**) - [US](http://shop.caldigit.com/us/index.php?route=product/product&product_id=170), [UK](http://shop.caldigit.com/uk/index.php?route=product/product&product_id=174)
    * Enables *stable* Dual Monitor Support for Engineers
      * Extended Desktop Support for DVI monitors requires ['active' displayport adaptors](http://www.cablematters.com/pc-33-33-cable-matters-gold-plated-displayport-to-dvi-male-to-female-adapter.aspx)
      * macOS does not support [Multi-Stream Transport over DisplayPort](https://www.displayport.org/cables/driving-multiple-displays-from-a-single-displayport-output/)
    * Recharges Laptop over USB-C
    * Provides USB-A support for peripherals
  * Belkin USB-C Multimedia Hub - [India](https://www.amazon.in/gp/product/B07Q3HP6KQ/)

##### Cables
  * AmazonBasics Premium HDMI Cable - [US](https://www.amazon.com/dp/B07KSD9DZ9/) - Supports 4K@60Hz
  * Rankie DisplayPort Cable - [UK](https://www.amazon.co.uk/gp/product/B00YOP0T7G/)
  * Tobo USB Type C (Thunderbolt 3) to DisplayPort Cable - [India](https://www.amazon.in/Thunderbolt-DisplayPort-Benfei-Display-Gold-Plated/dp/B06XFG1YKT) - Supports 4K@60Hz

##### Network Adaptors
  * TP-Link Powerline Adapter - [US](https://www.amazon.com/TP-LINK-Powerline-Pass-Through-TL-PA9020P-KIT/dp/B01H74VKZU), [UK](https://www.amazon.co.uk/TP-Link-TL-PA8033PKIT-Gigabit-Passthrough-Powerline/dp/B07GFHQXBP) - You'll want to aim for adaptors that can support the max ethernet standard or more of 1000mbps.

#### Notebook carrying bags
  * tomtoc 360° Protective Sleeve - [US](https://www.amazon.com/dp/B01N0TOQEO/)
  * NIDOO 15" - [Germany](https://www.amazon.de/dp/B072LVYC91/)
  * Mosiso Sleeve - [Australia](https://www.amazon.com.au/dp/B01N0W1YIK/)
  * SLOTRA Slim Anti-Theft Laptop Backpack - [UK](https://www.amazon.co.uk/SLOTRA-Lightweight-Resistant-Multipurpose-Anti-Theft/dp/B01DKLOOLG)

#### Monitors

##### Desktop monitors

Each person's monitor set up is a personal preference based on your own productivity. In recent years, many team members have decided to change from dual monitors to a single ultrawide monitor. Based on the table above, you can expense up to two 16:9 monitors (the reference price is $500 in the USA) or one ultrawide monitor (the reference price is $1000 in the USA). 

If you are an engineer or in a similar role, we encourage dual monitors or an ultrawide monitor. If you are not in an engineer(-like) role, you can make a choice based on your own personal preference and if it is required/helpful for your role and choose whether you want a single 16:9 monitor, dual 16:9 monitors, or a single ultrawide monitor.

When choosing monitors, it is important to keep in mind that there are different series of monitors with the same size. The Dell Professional (P#### models) and Ultrasharp (U models) series will have different price points based on the features and quality.

You may also want to buy a monitor arm for your desk that increases ergonomics. There are a wide variety of commodity brand monitor arms that are affordable such as the [Mount-It](https://www.amazon.com/stores/Mount-It/Mount-It/page/294B8EE4-088D-4426-891E-036605A6CABA) brand.

  * Ultrawide monitors (maximum $1,000 reimbursable):
    * Dell 34" P3421W - [US](https://www.dell.com/en-us/work/shop/dell-34-curved-usb-c-monitor-p3421w/apd/210-axqc/monitors-monitor-accessories)
    * Dell 34" U3421W - [US](https://www.dell.com/en-us/work/shop/dell-ultrasharp-34-curved-usb-c-hub-monitor-u3421we/apd/210-axqs/monitors-monitor-accessories)
    * Dell 38" U3821DW - [US](https://www.dell.com/en-us/work/shop/dell-ultrasharp-38-curved-usb-c-hub-monitor-u3821dw/apd/210-ayle/monitors-monitor-accessories)
    * Dell 49" U4919DW - [US](https://www.dell.com/en-us/work/shop/dell-ultrasharp-49-curved-monitor-u4919dw/apd/210-arnw/monitors-monitor-accessories)
    * Dell 40" 5K U4021QW - [US](https://www.dell.com/en-us/work/shop/dell-ultrasharp-40-curved-wuhd-monitor-u4021qw/apd/210-aykx)
    * LG 34" 34WL85C-B - [US](https://www.bhphotovideo.com/c/product/1457461-REG/lg_34wl85c_b_wl85c_34_ips_curved.html)
    * LG 38" 38UC99-W - [US](https://www.bhphotovideo.com/c/product/1279018-REG/lg_38uc99_w_37_5_21_9_wqhd.html)
    * LG 38" 38WN95C-W - [US](https://www.bhphotovideo.com/c/product/1567983-REG/lg_38wn95c_w_38_ultrawide_qhd_ips.html)
    * LG 49" 49WL95C-WE - [US](https://www.bhphotovideo.com/c/product/1622001-REG/lg_49wl95c_we_49_32_9_ultra_wide_dqhd_ips.html)
  * 4K 16:9 monitors (maximum $500 reimbursable):
    * LG 27UD58-B 27" 4K - [US](https://www.amazon.com/dp/B01IRQAYPE/)
    * LG 27UK850-W 27" 4K UHD with USB Type-C - [US](https://www.amazon.com/dp/B078GVTD9N/)
    * LG 27UL650-W 27" 4K UHD LED - [US](https://www.amazon.com/LG-27UL650-W-Monitor-DisplayHDR-White/dp/B07MKT2BNB)
    * BenQ PD2700U 27" 4K UHD IPS  - [US](https://www.amazon.com/BenQ-PD2700U-Professional-Monitor-3840x2160/dp/B07H9XP92N)
    * LG 27UL500W 27" 4K UHD IPS - [India](https://www.amazon.in/LG-4K-UHD-Monitor-Display-Freesync/dp/B07PGL2WVS/)
    * Dell U2720Q 27" 4K - [US](https://www.dell.com/en-us/work/shop/accessories/apd/210-avjv)
  * 1080p 16:9 monitors (maximum $500 reimbursable):
    * Dell P2418D 23.8" IPS QHD - [UK](https://www.amazon.co.uk/DELL-P2418D-23-8-Inch-Widescreen-Monitor/dp/B074MMR1V3)
    * Dell Ultra Sharp LED-Lit Monitor 25" 2560 X 1440 60 Hz IPS - [US](https://www.amazon.com/Dell-LED-Lit-Monitor-U2518D-Compatibility/dp/B075KGLYRL)
    * Acer S242HLDBID 24" - [Germany](https://www.amazon.de/dp/B01AJTVCA8/)
    * ASUS VS248H-P 24" 1080p - [US](https://www.amazon.com/dp/B0058UUR6E/)
    * ASUS PB277Q 27" 1440p - [US](https://www.amazon.com/gp/product/B01EN3Z7QQ/)
    * SAMSUNG F350 23.6" 1080p - [Germany](https://www.amazon.com.au/dp/B0771J3HXV/)
    * Lenovo ThinkVision P27h-10 27" 1440p - [Switzerland](https://www.digitec.ch/de/s1/product/lenovo-thinkvision-p27h-10-27-2560-x-1440-pixels-monitor-6611407)
      * Connects over USB-C and also acts as a hub with 4 USB3.0 ports (on the back), works great on Linux including audio passthrough!
    * Samsung 27" curved monitor [India](https://www.amazon.in/Samsung-inch-68-6-Curved-Monitor/dp/B01GFPGHVE)

##### Portable monitors
  * USB Touchscreen, 11.6" - [US](https://www.amazon.com/dp/B07FKJ6WP1/)
  * Kenowa 15.6" - [Germany](https://www.amazon.de/dp/B07FZ5PNDV/)
  * Asus Zenscreen 15,6" - [Netherlands](https://www.coolblue.nl/product/787645/asus-zenscreen-mb16ac.html)
  * Lepow 15.6" - [US](https://www.amazon.com/dp/B07RGPCQG1)
  * Duet App for iPad as a second monitor - [App store](https://apps.apple.com/app/duet-display/id935754064) and [Mac/Windows install](https://www.duetdisplay.com/)

##### Privacy screens
  * Macbook Pro 15" - [US](https://www.amazon.com/gp/product/B07GV71FF5/)
  * Macbook Pro 13" - [US](https://www.amazon.com/gp/product/B07GV71FF5/)

#### Headphones and earbuds
  * Mpow 059 Bluetooth Over Ear Headphones - [US](https://www.amazon.com/dp/B077XT82DD/)
  * JBL T450BT On-ear Bluetooth Headphones - [Germany](https://www.amazon.de/dp/B01M6WNWR6/)
  * OnePlus Bullets Wireless 2 - [India](https://www.oneplus.in/product/oneplus-bullets-wireless-2)
  * Logitech H390 Headset - [AU](https://www.amazon.com.au/Logitech-981-000485-USB-Headset-H390/dp/B077DC9XQH)
  * Apple AirPods are not recommended for Mac users - [US](https://www.apple.com/shop/accessories/all-accessories/headphones-speakers). Apple Airpods Pro can be purchased for folks who use them for work phone calls 
    * Note: Do not use AirPods in Zoom meetings for work if you are using a [Mac](https://www.apple.com/mac/) due to the audio quality issues. It is sometimes difficult to hear team members who are using the microphone on AirPods. AirPods also have limited battery life, and we often see problems with them not lasting through multiple meetings. Consider options that will last longer and not cause interruptions.

Keep in mind, open-ear headphones can often be worn longer than in-ear or closed headphones.

#### Webcams
  * Logitech C920 - [US](https://www.amazon.com/Logitech-Widescreen-Calling-Recording-Desktop/dp/B006JH8T3S), [UK](https://www.amazon.co.uk/Logitech-C920-Pro-Webcam-Recording/dp/B006A2Q81M?)
  * Logitech C525 - [India](https://www.amazon.in/Logitech-C525-HD-Webcam-Black/dp/B008QS9MRA)
  * Logitech C922 Pro Stream Webcam, HD 1080p - [India](https://www.amazon.in/Logitech-Stream-Webcam-Streaming-Recording/dp/B01M35CNS8)

#### Keyboards
  * Macally Bluetooth wireless keyboard - [US](https://www.amazon.com/Macally-Bluetooth-Computers-Rechargeable-Indicators/dp/B07K24ZLWC)
  * Logitech Ergo K860 Wireless Ergonomic Keyboard with Wrist Rest [US](https://www.amazon.com/gp/product/B07ZWK2TQT/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1)
  * Logitech G-613 Wireless Mechanical Keyboard with Lightspeed Technology [India](https://www.amazon.in/Logitech-Wireless-Gaming-Keyboard-Black/dp/B0767637QW)
  * Logitech MX Keys Advanced Illuminated Wireless Keyboard [India](https://www.amazon.in/Logitech-Advanced-Illuminated-Bluetooth-Responsive/dp/B08196YFMK)

#### U2F Tokens 
To understand more about where these can be used see the [Security Practices Page](https://about.gitlab.com/handbook/security/#two-factor-authentication).
  * Google Titan Security Key - [AT, CA, FR, DE, IT, JA, ES, CH, UK, US (excl. PR)](https://store.google.com/product/titan_security_key)
  * Yubikey - [Ships to most countries](https://www.yubico.com/store/)


#### Office Furniture

##### Desks
  * Autonomous SmartDesk 2 - Home Edition - [US and Europe](https://www.autonomous.ai/standing-desks/smart-desk-2-home)
  * Jarvis electric adjustable height desk - [US and Canada](https://www.fully.com/standing-desks/jarvis.html), [Europe](https://www.fully.eu/pages/jarvis-adjustable-standing-desks)

##### Chairs
  * Hbada Ergonomic Office Chair - [US](https://www.amazon.com/dp/B01N0XPBB3/)
  * INTEY Ergonomic Office Chair - [Germany](https://www.amazon.de/dp/B0744GS6LR/)
  * Kolina Ergonic Mesh Office Chair - [Australia](https://www.amazon.com.au/dp/B07BK7XDV8/)
  * IKEA MARKUS Office Chair - [UK](https://www.ikea.com/gb/en/products/chairs-stools-benches/desk-chairs/markus-office-chair-glose-black-art-20103101)
  * Featherlite Liberate Medium Back Desk Arm Chair - [India](https://www.amazon.in/Featherlite-Liberate-Medium-Chair-Black/dp/B07FPD46L3)

#### Other Accessories

##### Laptop Stands
  * BoYata Adjustable Laptop Stand - [UK](https://www.amazon.co.uk/gp/product/B07H89V3BB)
  * Roost Laptop Stand - [Worldwide](https://www.therooststand.com/)
    * If you are based in Australia, it is cheaper and faster to get it from the [Australian store.](https://www.therooststand.com.au/)
  * AmazonBasics Notebook Laptop Stand Arm Mount Tray - [US](https://www.amazon.com/gp/product/B010QZD6I6)

##### Wrist Rests
  * GIM Wrist Rest Set - [UK](https://www.amazon.co.uk/Keyboard-GIM-Support-Ergonomic-Computer/dp/B072K41FC1)

##### Whiteboards
  * Audio-Visual Direct White Magnetic Glass Dry-Erase Board Set - [US](https://www.amazon.com/dp/B00JNJWE3K)

##### Laptop Cooling Pads
  * Thermaltake 10-17" Laptop Cooling Pad with Temperature Sensors - [US](https://www.amazon.com/dp/B01FXDVE26)
  * havit 15.6-17" Laptop Cooling Pad - [US](https://www.amazon.com/dp/B07QKR6MB8)

#### Something else?
  * No problem! Consider adding it to this list if others can benefit from it.

Also see the [hardware expense table](/handbook/finance/expenses/#hardware) for a list of items and reference prices before purchasing.
Please reach out on the `#expense-reporting-inquires` channel if you are unsure what you would like to purchase is reimbursable.

